# Getting started

`npm install`

`npm start`

Running this will install all of the dependencies, then run the next build command. It will generate a `.next` directory, which will run the dev server on `PORT 3000`
