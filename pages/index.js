import React, { Component } from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import Select from 'react-select';
import axios from 'axios';
import Layout from '../components/layout.js';
import styled from 'styled-components';
import PreviewModal from '../components/preview-modal';

const CenterContainer = styled.div`
  margin: 7vh auto;
  width: 40%;
`;

const Selector = styled.div`
  margin: 1vh auto;
`;

const Input = styled.input`
  width: 100%;
  height: 36px;
  display: block;
  border: solid 1px #aacfe4;
  border-radius: 5px;
  padding: 0 10px;
  font-size: 14px;
`;

const Title = styled.h4`
  margin: 5px;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;
const Button = styled.button`
  flex: 1;
  height: 36px;
  border-radius: 10px;
  margin-right: 1vw;
`;

class Home extends Component {
  state = {
    title: '',
    description: '',
    content: '',
    previewUrl: '',
    image: '',
    contentType: '',
    preview: false,
    link: '',
    value: 'Copy Link'
  };

  onSelect = value => {
    this.setState({
      contentType: value,
      preview: false
    });
  };

  onPreview = () => {
    this.setState({
      preview: true
    });
  };

  handleChangeText = (e, name) => {
    e.preventDefault();
    this.setState({ [name]: e.target.value });
  };

  handleSubmit = () => {
    const {
      title,
      description,
      content,
      image,
      contentType,
      previewUrl
    } = this.state;

    const data = {
      title: title,
      description: description,
      content: content,
      image: image,
      contentType: contentType.value,
      previewUrl: previewUrl
    };

    //send to idk what we wanna call the endpoint bitch
    if (this.validated()) {
      axios
        .post('/content/create', data)
        .then(res => {
          const response = JSON.parse(res.data);

          this.setState(
            {
              title: '',
              description: '',
              content: '',
              previewUrl: '',
              image: '',
              contentType: '',
              preview: false,
              link: response.link
            },
            () => window.scrollTo(0, document.body.scrollHeight)
          );
        })
        .catch(err => console.log(err));
    }
  };

  validated = () => {
    const { title, content, image, contentType } = this.state;
    if (!title || !content || !image || !contentType) return false;
    return true;
  };

  copyLink = () => {};

  render() {
    const options = [
      { value: 'url', label: 'url' },
      { value: 'img', label: 'image link/gif' },
      { value: 'embed', label: 'embedded video' }
    ];

    const {
      title,
      description,
      image,
      previewUrl,
      contentType,
      content,
      link
    } = this.state;

    return (
      <Layout>
        <CenterContainer>
          <Title>Preview Title</Title>
          <Input
            type="text"
            onChange={e => this.handleChangeText(e, 'title')}
            placeholder="The title you want the recipient to see in the preview"
            value={title}
          />
          <Title>Preview Description</Title>
          <Input
            type="text"
            onChange={e => this.handleChangeText(e, 'description')}
            placeholder="Give it a good description, extra words to make it believable"
            value={description}
          />
          <Title>Preview Image</Title>
          <Input
            type="text"
            onChange={e => this.handleChangeText(e, 'image')}
            placeholder="Find an image on the interwebs, right click, and copy the image address"
            value={image}
          />
          <Title>Preview URL</Title>
          <Input
            type="text"
            onChange={e => this.handleChangeText(e, 'previewUrl')}
            placeholder="Fake url that's attached to the preview"
            value={previewUrl}
          />
          <Title>Content Source</Title>
          <Input
            type="text"
            onChange={e => this.handleChangeText(e, 'content')}
            placeholder="This can be an image link, an embeddedable, or URL"
            value={content}
          />
          <Title>Select Content Type</Title>
          <Selector>
            <Select
              value={contentType}
              onChange={this.onSelect}
              options={options}
            />
          </Selector>
          <br />
          <ButtonContainer>
            <Button onClick={this.handleSubmit} disabled={!this.validated()}>
              Create Link
            </Button>
            <PreviewModal
              title={title}
              description={description}
              previewUrl={previewUrl}
              image={image}
              contentType={contentType}
            />
          </ButtonContainer>
          {link ? (
            <h3>
              share dis link with frens: <br />
              <CopyToClipboard
                text={this.state.link}
                onCopy={() => this.setState({ value: this.state.link })}
              >
                <Button>hai copy this, fren</Button>
              </CopyToClipboard>
            </h3>
          ) : null}
        </CenterContainer>
      </Layout>
    );
  }
}

export default Home;
