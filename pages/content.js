import Head from 'next/head';
import Layout from '../components/layout.js';
import styled from 'styled-components';

const ContentStuff = styled.div`
  display: flex;
  flex: 1;
  margin: auto;
  justify-content: center;
  align-items: center;
`;

const Title = styled.h3`
  text-align: center;
`;

const Content = ({
  title,
  description,
  previewUrl,
  content,
  contentType,
  image
}) => {
  const contentFormatter = () => {
    switch (contentType) {
      case 'img':
        return <img src={content} />;
      case 'embed':
        return <div dangerouslySetInnerHTML={{ __html: content }} />;
      case 'url':
        return <iframe src={content} />;
      default:
        return <img src={content} />;
    }
  };

  return (
    <Layout>
      <Head>
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="og:url" content={previewUrl} />
        <meta property="og:image" content={image} />

        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
        <meta property="twitter:url" content={previewUrl} />
        <meta property="twitter:image" content={image} />
      </Head>
      <Title>Hi, pls enjoy content!</Title>
      <ContentStuff>{contentFormatter()}</ContentStuff>
    </Layout>
  );
};

Content.getInitialProps = async req => {
  if (req) {
    const {
      title,
      description,
      previewUrl,
      content,
      image,
      contentType
    } = req.query;

    return {
      title: title,
      description: description,
      previewUrl: previewUrl,
      content: content,
      image: image,
      contentType: contentType
    };
  }
};

export default Content;
