import Layout from '../components/layout.js';
import styled from 'styled-components';
import Profile from '../components/profile';
import { profiles } from '../constants/profiles';

const AboutContainer = styled.div`
  padding: 20px;
  width: 70vw;
  margin: 0 auto 10vh auto;
`;

const Title = styled.h3`
  text-align: center;
`;

const Bold = styled.b`
  text-decoration: underline;
`;

const Doge = styled.img`
  margin: 0 auto;
`;

const About = () => {
  return (
    <Layout>
      <AboutContainer>
        <Title>About This Website:</Title>
        <p>
          This site is to be used for whatever purpose you want. There are
          several use cases that we've thought about.
        </p>
        <ul>
          <li>
            <Bold>Tricking the recipient:</Bold> This is a bait-and-switch
            tactic, where the recipient believes he/she will be visiting a site
            whose content is displayed on the preview, only to be duped into
            viewing other content
          </li>
          <li>
            <Bold>Sharing content inconspicuously:</Bold> One can use this to
            mask the preview of the website. Due to the nature of some sites
            (pornographic, gore, furries, etc...), the preview content may be
            inappropriate for the environment, which may cause embarassment or
            be problematic. Use this to hide your links.
          </li>
        </ul>
        <p>Whatever you do, just have fun!</p>
        <Doge src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/Doge_homemade_meme.jpg/200px-Doge_homemade_meme.jpg" />
        <br />
        <br />
        <Title>About the Creators</Title>
        {profiles.map(({ name, description, picture }, key) => (
          <Profile
            name={name}
            description={description}
            picture={picture}
            key={key}
          />
        ))}
        <Title>Support the developers</Title>
      </AboutContainer>
    </Layout>
  );
};

export default About;
