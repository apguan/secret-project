import Link from 'next/link';
import styled, { keyframes } from 'styled-components';

const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
const NavbarContainer = styled(Row)`
  box-shadow: 1px 3px 5px gray;
  padding: 20px;
  background-color: #ffb6c1;
`;

const NavItems = styled(Row)`
  width: 40%;
  margin-right: 5%;
`;

const rainbow = keyframes`
  0%{color: orange;}	
  10%{color: purple;}	
  20%{color: red;}
  30%{color: CadetBlue;}
  40%{color: yellow;}
  50%{color: coral;}
  60%{color: green;}
  70%{color: cyan;}
  80%{color: DeepPink;}
  90%{color: DodgerBlue;}
  100%{color: orange;}
`;

const HeaderLink = styled.a`
  text-decoration: none;
  vertical-align: bottom;
  text-shadow: 2px 2px 4px #000000;

  /* Chrome, Safari, Opera */
  -webkit-animation: ${rainbow} 5s infinite;

  /* Internet Explorer */
  -ms-animation: ${rainbow} 5s infinite;

  /* Standar Syntax */
  animation: ${rainbow} 5s infinite;

  &:hover {
    cursor: pointer;
  }
`;

const shimmer = keyframes`
  0% {
    background-position: -500px 0;
  }
  100% {
    background-position: 500px 0;
  }
`;

const Logo = styled(HeaderLink)`
  font-family: 'Lobster', cursive;
  font-size: 20px;
  color: #fb551c;

  background-image: -moz-linear-gradient(
    160deg,
    rgba(255, 255, 255, 0) 0%,
    rgba(255, 255, 255, 0) 25%,
    rgba(255, 255, 255, 0.85) 60%,
    rgba(255, 255, 255, 0) 100%
  );
  background-image: -webkit-gradient(
    linear,
    left top,
    right top,
    color-stop(0%, rgba(255, 255, 255, 0)),
    color-stop(25%, rgba(255, 255, 255, 0)),
    color-stop(60%, rgba(255, 255, 255, 0.85)),
    color-stop(100%, rgba(255, 255, 255, 0))
  );
  background-image: -webkit-linear-gradient(
    160deg,
    rgba(255, 255, 255, 0) 0%,
    rgba(255, 255, 255, 0) 25%,
    rgba(255, 255, 255, 0.85) 60%,
    rgba(255, 255, 255, 0) 100%
  );
  background-image: -o-linear-gradient(
    160deg,
    rgba(255, 255, 255, 0) 0%,
    rgba(255, 255, 255, 0) 25%,
    rgba(255, 255, 255, 0.85) 60%,
    rgba(255, 255, 255, 0) 100%
  );
  background-image: -ms-linear-gradient(
    160deg,
    rgba(255, 255, 255, 0) 0%,
    rgba(255, 255, 255, 0) 25%,
    rgba(255, 255, 255, 0.85) 60%,
    rgba(255, 255, 255, 0) 100%
  );
  background-image: linear-gradient(
    160deg,
    rgba(255, 255, 255, 0) 0%,
    rgba(255, 255, 255, 0) 25%,
    rgba(255, 255, 255, 0.85) 60%,
    rgba(255, 255, 255, 0) 100%
  );
  background-repeat: repeat-y;
  background-position: 20px 0;
  animation: ${shimmer} 3s linear infinite;
`;

const Navbar = () => {
  return (
    <NavbarContainer>
      <Logo href="/">Diddle</Logo>
      <NavItems>
        <Link href="/">
          <HeaderLink>Home</HeaderLink>
        </Link>
        <Link href="/content/random">
          <HeaderLink>Random Content</HeaderLink>
        </Link>
        <Link href="/about">
          <HeaderLink>About</HeaderLink>
        </Link>
      </NavItems>
    </NavbarContainer>
  );
};
export default Navbar;
