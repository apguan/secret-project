import React from 'react';
import styled from 'styled-components';
import Modal from 'react-modal';

Modal.setAppElement('body');

const Button = styled.button`
  height: 36px;
  border-radius: 10px;
  width: 100%;
`;

const Image = styled.img`
  object-fit: cover;
  height: 48%;
  width: 100%;
  border: none;
`;

const Details = styled.div`
  font-family: system-ui, -apple-system, BlinkMacSystemFont, '.SFNSText-Regular',
    sans-serif;
  background-color: #e9ebee;
  border: none;
  margin-top: -1vh;
  padding: 1.5vh;
`;

const Title = styled.h2`
  color: black;
  font-size: 1.5em;
  margin-top: 5px;
`;

const Url = styled.div`
  color: #828282;
  font-size: 12px;
`;

const Description = styled.div`
  color: #606770;
  font-size: 12px;
`;

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    height: '500px',
    width: '400px',
    border: 'none',
    boxShadow: '1px 1px 3px gray'
  }
};

export default class PreviewModal extends React.Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  getDomain(url) {
    if (url.indexOf('//') !== -1) {
      return url.split('/')[2].toUpperCase();
    }
    return url.split('/')[0].toUpperCase();
  }

  render() {
    const { title, description, image, previewUrl } = this.props;

    return (
      <div style={{ width: '40%', flex: 1 }}>
        <Button onClick={this.openModal}>Preview</Button>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <Title>Facebook Post Preview</Title>
          <p>
            Note: We don't handle the preview rendering, but as long as you see
            the image, your preview will show up properly
          </p>
          <br />
          <Image src={image} />
          <Details>
            <Url>{this.getDomain(previewUrl)}</Url>
            <Title style={{ marginTop: 0 }}>{title}</Title>
            <Description>{description}</Description>
          </Details>
        </Modal>
      </div>
    );
  }
}
