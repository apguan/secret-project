import React from 'react';
import styled from 'styled-components';

const ProfileContainer = styled.div``;

const Info = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const Picture = styled.img`
  margin-right: 20px;
  background-color: #fff;
  width: 10vw;
  height: 10vw;
`;

const Description = styled.div``;

const Profile = props => (
  <ProfileContainer>
    <h2>{props.name}</h2>
    <Info>
      <Picture src={props.picture} />
      <Description>{props.description}</Description>
    </Info>
  </ProfileContainer>
);

export default Profile;
