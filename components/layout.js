import React, { Fragment, Component } from 'react';
import Head from 'next/head';
import ReactGA from 'react-ga';
import Navbar from './navbar.js';

const style = {
  fontFamily: 'Comic Sans MS',
  height: '100vh'
};

class Layout extends Component {
  componentDidMount() {
    ReactGA.initialize('UA-144099730-1');
    ReactGA.pageview(window.location.pathname + window.location.search);
  }

  render() {
    return (
      <Fragment>
        <Head>
          <link
            href="http://allfont.net/allfont.css?fonts=comic-sans-ms"
            rel="stylesheet"
            type="text/css"
          />
          <link
            href="https://fonts.googleapis.com/css?family=Lobster&display=swap"
            rel="stylesheet"
          />
        </Head>
        <style jsx global>{`
          body {
            margin: 0;
            background-color: #fdfd96;
            height: 100vh;
            font-family: Comic Sans MS;
          }
        `}</style>
        <Navbar />
        <div style={style}>{this.props.children}</div>
      </Fragment>
    );
  }
}

export default Layout;
