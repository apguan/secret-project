import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Icon = props => (
  <FontAwesomeIcon icon={props.icon} styles={props.style} />
);
