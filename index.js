const express = require('express');
const next = require('next');
const bodyParser = require('body-parser');
const dev = process.env.NODE_ENV !== 'production';
const app = next({
  dev
});
const handle = app.getRequestHandler();

const port = parseInt(process.env.PORT, 10) || 3000;

//db stuff here
const url = require('./secret.js');
const mongoose = require('mongoose');
mongoose.connect(url.DB_URL, {
  useNewUrlParser: true
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log('connected to the database');
});

const contentSchema = mongoose.Schema({
  id: String,
  title: String,
  description: String,
  previewUrl: String,
  contentType: String,
  content: String,
  image: String,
  serial: Number
});

const Content = mongoose.model('Content', contentSchema);

const saveContent = content => {
  Content.estimatedDocumentCount({}, function(err, count) {
    const saveContent = Object.assign({ serial: count }, content);
    newContent = new Content(saveContent);
    if (err) console.log(err);
    else newContent.save();
  });
};

const getContent = (id, callback) => {
  Content.findOne({ id }).exec((err, data) => {
    if (err) callback(err, null);
    else {
      callback(null, data);
    }
  });
};

const getRandom = callback => {
  Content.estimatedDocumentCount({}, function(err, count) {
    if (err) console.log(err, ' this is teh errror');
    else {
      const serial = Math.floor(Math.random() * count);
      Content.findOne({ serial }, function(err, data) {
        if (err) callback(err, null);
        else callback(null, data);
      });
    }
  });
};

//prep the server m'lord!
app
  .prepare()
  .then(() => {
    const server = express();
    server.use(bodyParser.json());

    //process these mofo requests

    server.get('/content/random', (req, res) => {
      const contentPage = '/content';
      return getRandom(function(err, data) {
        if (err) console.log(err, 'we are getting an error');
        else return app.render(req, res, contentPage, data);
      });
    });

    server.get('/content/:id', (req, res) => {
      console.log('this request has been received', req);
      const contentPage = '/content';
      if (typeof req.params.id === 'string') {
        return getContent(req.params.id, function(err, data) {
          if (err) console.log(err);
          else app.render(req, res, contentPage, data);
        });
      }
    });

    server.get('*', (req, res) => {
      handle(req, res);
    });

    server.post('/content/create', (req, res) => {
      const currentTime = String(new Date().getTime()); //in nano seconds, very small chance of collision
      const data = {
        id: currentTime,
        title: req.body.title,
        description: req.body.description,
        image: req.body.image,
        previewUrl: req.body.previewUrl,
        contentType: req.body.contentType,
        content: req.body.content
      };

      console.log(data);
      saveContent(data);

      res.header('Content-Type', 'application/json');
      res.json(
        JSON.stringify({ link: `${req.hostname}/content/${currentTime}` })
      );
      res.end();
    });

    server.listen(port, err => {
      if (err) throw err;
      console.log(`🤘 on http://localhost:${port}`);
    });
  })
  .catch(err => {
    console.log(err.stack);
    process.exit(1);
  });
